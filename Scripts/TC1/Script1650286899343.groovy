import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import io.appium.java_client.android.AndroidKeyCode as AndroidKeyCode
import com.google.common.collect.ImmutableMap as ImmutableMap;
import MyKeyword as MyKeyword

Mobile.startExistingApplication('com.appsgallery.sagar.loginregistrationexample', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('btnLogin'), 10)

Mobile.verifyElementVisible(findTestObject('btnRegis'), 10)

Mobile.tap(findTestObject('btnRegis'), 10)

Mobile.verifyElementVisible(findTestObject('regisEtUsername'), 10)

def randomMail = CustomKeywords.'MyKeyword.randName'()

Mobile.setText(findTestObject('regisEtUsername'), randomMail, 10)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('regisEtPassword'), 'secret', 0)

Mobile.hideKeyboard()

Mobile.setText(findTestObject('regisEtConfirmPassword'), 'secret', 10)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('regisBtnCreate'), 10)

AppiumDriver<?> driver = MobileDriverFactory.getDriver()

def toast = driver.findElementByXPath('//*[@class = \'android.widget.Toast\' and (@text = \'Account Successfully Created \' or . = \'Account Successfully Created \')]')

'Assert sukses register : cek toast'
println('Toast element: ' + toast)

println(toast.getText())

if (toast == null) {
    KeywordUtil.markFailed('ERROR: Toast object not found!')
}

Mobile.pressBack()

Mobile.verifyElementVisible(findTestObject('btnLogin'), 10)

'Konsep : setelah buka browser , tambahkan fungsi assert untuk klik link dan verifikasi sesuai kebutuhan'
Mobile.startExistingApplication('com.android.chrome', FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Chrome/eTchrome'), 25)

Mobile.tap(findTestObject('Chrome/etChrome'), 4)

Mobile.verifyElementVisible(findTestObject('Chrome/etUrl'), 10)

Mobile.sendKeys(findTestObject('Chrome/etUrl'), 'https://www.mailinator.com/v4/public/inboxes.jsp?to='+randomMail, FailureHandling.STOP_ON_FAILURE)

driver.executeScript("mobile: performEditorAction", ImmutableMap.of("action", "search"));


'Asumsi, setelah sukses verifikasi link via browser maka akan kembali ke apps kemudian coba login'
Mobile.startExistingApplication('com.appsgallery.sagar.loginregistrationexample', FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('etUserName'), 10)

Mobile.setText(findTestObject('etUserName'), randomMail, 5)

Mobile.verifyElementVisible(findTestObject('etPassword'), 10)

Mobile.setText(findTestObject('etPassword'), 'secret', 5)

Mobile.hideKeyboard()

Mobile.verifyElementVisible(findTestObject('btnLogin'), 10)

Mobile.doubleTap(findTestObject('btnLogin'), 10)

'Assert sukses login'
Mobile.verifyElementVisible(findTestObject('tvSuccessLogin'), 10)

