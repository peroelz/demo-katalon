**Simple mobile automation demo, using katalon IDE**\
Scenario:\
-Register\
-Assert register success\
-Open browser\
-Login using previous data\
-Assert login success

Step:\
-Clone project\
-Download faker.jar from https://repo1.maven.org/maven2/com/github/javafaker/javafaker/1.0.2/javafaker-1.0.2.jar\
-Import .jar file to your project\ 
-Install demo apps from link below on your device:\
https://apkpure.com/login-registration-example/com.appsgallery.sagar.loginregistrationexample/download?from=details
